# 0.15.0

**Breaking changes**: update `vmol-auth` dependency to version 0.6

# 0.12.0

**Breaking changes**: update `jsonwebtoken` from version 8 to 9.

# 0.9.0

**Breaking changes**: Invitations use now an array of roles.

- in `create` action, the `role` param is replaced by `roles`
- the `accept` action returns an array of roles
- each role object returned by actions has now a `roles` key, which is an array. The `role` key doesn't exist anymore
