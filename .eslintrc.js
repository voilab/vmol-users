module.exports = {
    'root': true,
    'env': {
        'node': true,
        'es6': true,
        'jest': true
    },
    'extends': 'eslint:recommended',
    'parserOptions': {
        'sourceType': 'module',
        'ecmaVersion': '2020'
    },
    'rules': {
        'indent': [
            'warn',
            4,
            { 'SwitchCase': 1 }
        ],
        'quotes': [
            'warn',
            'single'
        ],
        'semi': [
            'error',
            'always'
        ],
        'no-var': [
            'error'
        ],
        'no-console': [
            'error'
        ],
        'no-unused-vars': [
            'warn'
        ],
        'no-mixed-spaces-and-tabs': [
            'warn'
        ]
    }
};
