'use strict';

const jwt = require('jsonwebtoken');

const { MoleculerClientError } = require('moleculer').Errors;

module.exports = {
    methods: {
        /**
         * Sign a new token.
         *
         * @param {String} subject The token subject
         * @param {Object} payload The token payload
         * @param {String} expiresIn The token expiration
         * @param {String|Array} audience The token subject
         *
         * @returns {String} Encoded and signed JWT
         */
        signToken(subject, expiresIn, payload = {}, audience) {
            const params = {
                algorithm: this.settings.jwt.algorithm,
                issuer: this.settings.jwt.issuer,
                subject
            };

            if (expiresIn) {
                params.expiresIn = expiresIn;
            }

            if (audience !== undefined) {
                params.audience = audience;
            }

            // Synchronous call, because: https://github.com/auth0/node-jsonwebtoken/issues/111
            return jwt.sign(
                payload,
                this.settings.jwt.privateKey,
                params
            );
        },

        /**
         * Verify a token.
         *
         * @param {String} token JWT
         * @param {String} subject The token subject, optional
         * @param {Object} payload The token payload, optional
         *
         * @returns {Object} Decoded token content
         */
        verifyToken(token, subject, audience) {
            const params = {
                issuer: this.settings.jwt.issuer
            };

            if (subject !== undefined) {
                params.subject = subject;
            }

            if (audience !== undefined) {
                params.audience = audience;
            }

            // Synchronous call, because: https://github.com/auth0/node-jsonwebtoken/issues/111
            try {
                return jwt.verify(
                    token,
                    this.settings.jwt.publicKey,
                    params
                );
            } catch(e) {
                if (e instanceof jwt.TokenExpiredError) {
                    throw new MoleculerClientError('Expired token', 401, 'TOKEN_EXPIRED_ERROR', {
                        type: 'jwtExpired',
                        message: `Users: ${e.message}`
                    });
                }

                if (e instanceof jwt.JsonWebTokenError) {
                    throw new MoleculerClientError('Invalid token', 401, 'INVALID_TOKEN_ERROR', {
                        type: 'jwtInvalid',
                        message: `Users: ${e.message}`
                    });
                }

                throw new MoleculerClientError('General token error', 401, 'TOKEN_GENERIC_ERROR', {
                    type: 'jwtUnknown',
                    message: `Users: ${e.message}`
                });
            }
        }
    }
};
