'use strict';

const { BaseModel } = require('@voilab/vmol-objection');

const UserModel = require('./UserModel');

const formats = require('./lib/formats');

class UsersRoleModel extends BaseModel {
    static get tableName() {
        return 'users_roles';
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['role'],
            properties: {
                id: formats.MODEL_ID,
                user_id: formats.MODEL_ID,
                tenant_id: formats.MODEL_NULLABLE_ID,
                role: { type: 'string' }
            }
        };
    }

    static get relationMappings() {
        return {
            user: {
                relation: BaseModel.BelongsToOneRelation,
                modelClass: UserModel,
                join: {
                    from: 'users_roles.user_id',
                    to: 'users.id'
                }
            }
        };
    }

    $beforeUpdate() {
        this.updated_at = new Date().toISOString();
    }
}

module.exports = UsersRoleModel;
