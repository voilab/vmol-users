'use strict';

const { BaseModel } = require('@voilab/vmol-objection');
const { fn } = require('objection');

const UserModel = require('./UserModel');

const formats = require('./lib/formats');

class SessionModel extends BaseModel {
    static get tableName() {
        return 'users_sessions';
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['user_id'],
            properties: {
                id: formats.MODEL_ID,
                secret: { type: 'string', format: 'uuid' },
                user_id: formats.MODEL_ID,
                delegation_id: formats.MODEL_ID,
                kind: { type: 'string' },
                data: { type: 'object' },
                revoked_at: { type: 'string', format: 'date-time' },
                lastseen_at: { type: 'string', format: 'date-time' }
            }
        };
    }

    static get relationMappings() {
        return {
            user: {
                relation: BaseModel.BelongsToOneRelation,
                modelClass: UserModel,
                join: {
                    from: 'users_sessions.user_id',
                    to: 'users.id'
                }
            }
        };
    }

    $beforeUpdate() {
        this.updated_at = fn.now();
    }
}

module.exports = SessionModel;
