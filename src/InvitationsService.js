'use strict';

const { DBMiddleware } = require('@voilab/vmol-objection');
const { ValidationError } = require('moleculer').Errors;

const formats = require('./lib/formats');

/**
 * Sessions Service definition
 */
module.exports = {
    /**
     * Mixins
     */
    mixins: [DBMiddleware],

    /**
     * Settings
     */
    settings: {
        /**
         * @type {String} Users management service name
         */
        usersService: 'users'
    },

    /**
     * Actions
     */
    actions: {
        /**
         * Create an invitation
         *
         * Invite a user to access a tenant.
         *
         * @actions
         *
         * @param {String} email User email to invite
         * @param {String} tenant_id The tenant on which the user is invited
         * @param {String|Array<String>} roles The roles the user will receive
         *
         * @returns {Object} Invitation
         */
        create: {
            params: {
                email: 'email|normalize',
                tenant_id: formats.VALIDATOR_ID,
                roles: ['string', { type: 'array', min: 1, unique: true, items: 'string' }]
            },

            async handler(ctx) {
                const roles = Array.isArray(ctx.params.roles) ? ctx.params.roles : [ctx.params.roles];

                const validRoles = roles.filter(r => !!r);
                if (!validRoles.length) {
                    throw new ValidationError('At least one valid role must be provided for [roles]', null, [{
                        type: 'arrayEnum',
                        message: 'No role given',
                        field: 'roles',
                        actual: [],
                        expected: ['valid-role']
                    }]);

                }

                return this.models.InvitationModel.query()
                    .insert({
                        invited_by_user_id: ctx.locals.authn.id,
                        tenant_id: ctx.params.tenant_id,
                        email: ctx.params.email,
                        roles: validRoles
                    })
                    .returning('*')
                    .then(this.filterSecureFields);
            }
        },

        /**
         * List pending invitations by tenant
         *
         * @actions
         *
         * @param {String} tenant_id The tenant for which the invitations are sent
         * @param {Boolean} include_revoked Include revoked invitations
         * @param {Boolean} include_accepted Include accepted invitations
         *
         * @returns {Array} Invitations
         */
        listByTenant: {
            params: {
                tenant_id: formats.VALIDATOR_ID,
                include_revoked: { type: 'boolean', default: false },
                include_accepted: { type: 'boolean', default: false }
            },

            async handler(ctx) {
                const conds = {
                    tenant_id: ctx.params.tenant_id
                };

                if (!ctx.params.include_revoked) {
                    conds.revoked_at = null;
                }
                if (!ctx.params.include_accepted) {
                    conds.accepted_at = null;
                }

                return this.models.InvitationModel.query()
                    .where(conds)
                    .then(this.filterSecureFields);
            }
        },

        /**
         * List own pending invitations
         *
         * @actions
         *
         * @param {Boolean} include_revoked Include revoked invitations
         * @param {Boolean} include_accepted Include accepted invitations
         *
         * @returns {Array} Invitations
         */
        listOwn: {
            params: {
                include_revoked: { type: 'boolean', default: false },
                include_accepted: { type: 'boolean', default: false }
            },

            async handler(ctx) {
                const conds = {
                    email: ctx.locals.authn.email
                };

                if (!ctx.params.include_revoked) {
                    conds.revoked_at = null;
                }
                if (!ctx.params.include_accepted) {
                    conds.accepted_at = null;
                }

                return this.models.InvitationModel.query()
                    .where(conds)
                    .then(this.filterSecureFields);
            }
        },

        /**
         * Revoke an invitation
         *
         * The current session MUST either have admin access to the tenant OR be the invited user.
         *
         * @actions
         *
         * @param {String} invitation_id Invitation ID
         */
        revoke: {
            params: {
                invitation_id: formats.VALIDATOR_ID
            },

            async handler(ctx) {
                return this.models.InvitationModel.query()
                    .where('id', ctx.params.invitation_id)
                    .where((builder) =>
                        builder.whereIn('tenant_id', this.getTenantIds(ctx, 'admin'))
                            .orWhere('email', ctx.locals.authn.email)
                    )
                    .patch({
                        revoked_at: (new Date()).toISOString()
                    })
                    .throwIfNotFound();
            }
        },

        /**
         * Accept an invitation
         *
         * The current session MUST be the invited user
         *
         * @actions
         *
         * @param {String} invitation_id Invitation ID
         *
         * @returns {Array<Object>} UsersRole objects
         */
        accept: {
            params: {
                invitation_id: formats.VALIDATOR_ID
            },

            async handler(ctx) {
                const invitation = await this.models.InvitationModel.query()
                    .findOne({
                        id: ctx.params.invitation_id,
                        email: ctx.locals.authn.email   // Ensure that the invitation is for the current user
                    })
                    .throwIfNotFound();

                const roles = await ctx.mcall(invitation.roles.map(role => ({
                    action: `${this.settings.usersService}.addRole`,
                    params: {
                        user_id: ctx.locals.authn.id,   // Matches the email on the token
                        role_name: role,
                        tenant_id: invitation.tenant_id
                    }
                })));

                return this.models.InvitationModel.query()
                    .where('id', invitation.id)
                    .patch({
                        accepted_at: (new Date()).toISOString()
                    })
                    .then(() => roles);
            }
        }
    }
};
