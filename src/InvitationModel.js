'use strict';

const { BaseModel } = require('@voilab/vmol-objection');

const UserModel = require('./UserModel');

const formats = require('./lib/formats');

class InvitationModel extends BaseModel {
    static get tableName() {
        return 'users_invitations';
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: [
                'invited_by_user_id',
                'tenant_id',
                'email',
                'roles'
            ],
            properties: {
                id: formats.MODEL_ID,
                invited_by_user_id: formats.MODEL_ID,
                tenant_id: formats.MODEL_ID,
                email: { type: 'string', format: 'email', maxLength: 254 },
                roles: { elements: { type: 'string', maxLength: 127 } },
                revoked_at: { type: 'string', format: 'date-time' },
                accepted_at: { type: 'string', format: 'date-time' }
            }
        };
    }

    static get relationMappings() {
        return {
            user: {
                relation: BaseModel.BelongsToOneRelation,
                modelClass: UserModel,
                join: {
                    from: 'users_invitations.invited_by_user_id',
                    to: 'users.id'
                }
            }
        };
    }

    $beforeUpdate() {
        this.updated_at = new Date().toISOString();
    }
}

module.exports = InvitationModel;
