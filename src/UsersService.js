'use strict';

const { ValidationError } = require('moleculer').Errors;
const { DBMiddleware } = require('@voilab/vmol-objection');
const Scrypt = require('scrypt-kdf');
const stringEntropy = require('fast-password-entropy');

const jwtMixin = require('./mixins/jwt.mixin');
const formats = require('./lib/formats');

/**
 * VMolUsers Service definition
 */
module.exports = {
    /**
     * Mixins
     */
    mixins: [ DBMiddleware, jwtMixin ],

    /**
     * Settings
     */
    settings: {
        /**
         * Secret parameters will be ignored from the logs and denied to other services.
         */
        $secureSettings: ['jwt.privateKey', 'secureFields'],

        /**
         * Set the minimum password entropy as bits.
         *
         * @see https://en.wikipedia.org/wiki/Password_strength#Entropy_as_a_measure_of_password_strength
         */
        minPasswordEntropy: 50,

        /**
         * Scrypt cost parameter
         * /!\ Warning /!\ Do *not* change if passwords are already in the database. A migration will be required.
         *
         * @see https://blog.filippo.io/the-scrypt-parameters/
         */
        scryptN: 15,

        /**
         * Default role added to a new user. If null, no default role will be added
         * @type {String} Role name
         */
        defaultUserRole: null,

        /**
         * @type {Array} Hide fields from the responses.
         */
        secureFields: [ 'password', 'roles' ],

        jwt: {
            /**
             * @type {String} JWT Private key in PEM format
             */
            privateKey: null,

            /**
             * @type {String} JWT Public key in PEM format
             */
            publicKey: null,

            /**
             * @type {String} JWT algorithm
             */
            algorithm: 'ES256',

            /**
             * @type {String} JWT Expiration time for validation and recovery
             */
            expiresIn: '24h',

            /**
             * @type {String} JWT Issuer
             */
            issuer: 'UsersService'
        },

        /**
         * @type {String} Password recovery token audience
         */
        recoveryTokenAudience: 'passwordRecovery',
        recoveryTokenExpiresIn: '24h',

        /**
         * @type {String} Account validation token audience
         */
        validationTokenAudience: 'accountValidation',
        validationTokenExpiresIn: '24h'
    },

    /**
     * Actions
     */
    actions: {
        /**
         * Create a new user.
         *
         * @actions
         *
         * @param {String} email User's email
         * @param {String} password User's password
         *
         * @returns {Object} Saved user.
         */
        create: {
            params: {
                email: 'email|normalize',
                password: 'string'
            },

            async handler(ctx) {
                this.assertPasswordComplexity(ctx.params.password);

                ctx.params.password = await this.passwordKDF(ctx.params.password);

                return this.models.UserModel.query()
                    .insert(ctx.params)
                    .returning('*')
                    .then((user) => {
                        if (this.settings.defaultUserRole !== null && typeof this.settings.defaultUserRole === 'string') {
                            return this._addRole(
                                user.id,
                                this.settings.defaultUserRole,
                                null
                            ).then(() => {
                                return user;
                            });
                        }

                        return user;
                    })
                    .then(this.filterSecureFields)
                    .catch((e) => {
                        if (e.name !== 'ConstraintViolationError') {
                            this.logger.error(e);
                            throw e;
                        }

                        throw new ValidationError(
                            'Parameters validation error!',
                            null,
                            {
                                type: 'emailUnique',
                                message: 'The email provided is already used.',
                                field: 'email'
                            });
                    });
            }
        },

        /**
         * Get a user by its credentials
         *
         * @actions
         *
         * @param {String} email User's email
         * @param {String} password User's password
         *
         * @returns {Object} User with roles
         */
        getByCredentials: {
            params: {
                email: 'email|normalize',
                password: 'string'
            },

            handler(ctx) {
                return this._getOneWithRolesByEmail(ctx.params.email)
                    .then((user) => {
                        return this.passwordVerify(user.password, ctx.params.password)
                            .then((passwordOk) => {
                                if (!passwordOk) {
                                    throw new Error('invalidCredentials');
                                }

                                return user;
                            });
                    })
                    .catch(e => {
                        if (e.type == 'NotFound' || e.message == 'invalidCredentials') {
                            throw new ValidationError(
                                'Parameters validation error!',
                                null,
                                {
                                    type: 'invalidCredentials',
                                    message: 'The user email or password is unknown.',
                                    field: 'password'
                                });
                        }

                        throw e;
                    });
            }
        },

        /**
         * Generate an account recovery token.
         *
         * This token can be used to call the `resetPassword` action.
         * Be sure to send this token securely to the end user and don't expose this action unprotected.
         * The token will allow a full account takeover.
         *
         * @actions
         *
         * @param {String} email The account's email address
         *
         * @returns {String} JWT.
         */
        createRecoveryToken: {
            params: {
                email: 'email|normalize',
                data: 'object|optional'
            },

            handler(ctx) {
                return this.signToken(
                    ctx.params.email,
                    this.settings.recoveryTokenExpiresIn,
                    ctx.params.data || {},
                    this.settings.recoveryTokenAudience
                );
            }
        },

        /**
         * Reset a user's password.
         *
         * @actions
         *
         * @param {String} token A valid JWT generated by `createRecoveryToken`
         * @param {String} password The new user password
         */
        resetPassword: {
            params: {
                token: 'string',
                password: 'string'
            },

            async handler(ctx) {
                const token = this.verifyToken(ctx.params.token, undefined, this.settings.recoveryTokenAudience);

                this.assertPasswordComplexity(ctx.params.password);
                const password = await this.passwordKDF(ctx.params.password);

                return this._getOneWithRolesByEmail(token.sub)
                    .then((user) => {
                        return this.models.UserModel.query()
                            .throwIfNotFound()
                            .findById(user.id)
                            .patch({password})
                            .returning('*');
                    })
                    .then(() => {
                        return { result: 'ok' };
                    });
            }
        },

        /**
         * Generate an email validation token.
         *
         * The token is aimed to be used when validating a user's email address and can be verified with `verifyValidationToken`.
         *
         * @actions
         *
         * @param {String} email The account's email address
         *
         * @returns {String} JWT.
         */
        createValidationToken: {
            params: {
                email: 'email|normalize',
                data: 'object|optional'
            },

            handler(ctx) {
                return this.signToken(
                    ctx.params.email,
                    this.settings.validationTokenExpiresIn,
                    ctx.params.data || {},
                    this.settings.validationTokenAudience
                );
            }
        },

        /**
         * Verify a validation token generated by `createValidationToken`.
         *
         * @actions
         *
         * @param {String} token A valid JWT generated by `createValidationToken`
         */
        verifyValidationToken: {
            params: {
                token: 'string'
            },

            handler(ctx) {
                return this.verifyToken(ctx.params.token, undefined, this.settings.validationTokenAudience);
            }
        },

        /**
         * Get a single user
         *
         * @actions
         *
         * @param {String} id
         * @param {String} tenant_id Filter by tenant
         * @param {String} role Filter by role
         *
         * @returns {Object} User
         * @throws {NotFoundError} - Entity not found
         */
        get: {
            params: {
                id: formats.VALIDATOR_ID,
                tenant_id: formats.VALIDATOR_OPTIONAL_ID,
                role: 'string|optional'
            },

            async handler(ctx) {
                const query = this.models.UserModel.query()
                    .findOne({ 'users.id': ctx.params.id });

                if (ctx.params.tenant_id || ctx.params.role) {
                    query.withGraphJoined('roles');
                }

                if (ctx.params.tenant_id) {
                    query.where({'roles.tenant_id': ctx.params.tenant_id});
                }

                if (ctx.params.role) {
                    query.where({'roles.role': ctx.params.role});
                }

                return query.throwIfNotFound()
                    .then(this.filterSecureFields);
            }
        },

        /**
         * Get a user by ID with its roles
         *
         * @actions
         *
         * @param {String} id User id
         *
         * @returns {Object} User with roles
         */
        getWithRoles: {
            params: {
                id: formats.VALIDATOR_ID
            },

            handler(ctx) {
                return this.models.UserModel.query()
                    .findOne({ 'users.id': ctx.params.id })
                    .withGraphJoined('roles')
                    .throwIfNotFound()
                    .then(users => this.filterSecureFields(users, ['roles']));
            }
        },

        /**
         * Get a user by its email
         *
         * @actions
         *
         * @param {String} email The user's email address
         * @param {String} tenant_id Filter by tenant
         * @param {String} role Filter by role
         *
         * @returns {Object} user
         */
        getOneByEmail: {
            params: {
                email: 'email|normalize',
                tenant_id: formats.VALIDATOR_OPTIONAL_ID,
                role: 'string|optional'
            },

            handler(ctx) {
                const query = this.models.UserModel.query()
                    .findOne({ 'users.email': ctx.params.email });

                if (ctx.params.tenant_id || ctx.params.role) {
                    query.withGraphJoined('roles');
                }

                if (ctx.params.tenant_id) {
                    query.where({ 'roles.tenant_id': ctx.params.tenant_id });
                }

                if (ctx.params.role) {
                    query.where({ 'roles.role': ctx.params.role });
                }

                return query.throwIfNotFound()
                    .then(this.filterSecureFields);
            }
        },

        /**
         * Get a user by its token
         *
         * @actions
         *
         * @returns {Object} user
         */
        getByToken(ctx) {
            return this.models.UserModel.query()
                .throwIfNotFound()
                .findById(ctx.locals.authn.id)
                .then(this.filterSecureFields);
        },

        /**
         * List users by tenant and optionaly filter by role
         *
         * @actions
         *
         * @param {String} tenant_id
         * @param {String} role
         */
        listByTenant: {
            params: {
                tenant_id: formats.VALIDATOR_ID,
                role: 'string|optional'
            },

            handler(ctx) {
                const conditions = {
                    'roles.tenant_id': ctx.params.tenant_id
                };

                if (ctx.params.role) {
                    conditions['roles.role'] = ctx.params.role;
                }

                return this.models.UserModel.query()
                    .withGraphJoined('roles')
                    .where(conditions)
                    .then(this.filterSecureFields);
            }
        },

        /**
         * List users and their roles by tenant and optionaly filter by role
         *
         * @actions
         *
         * @param {String} tenant_id
         * @param {String} role
         */
        listByTenantWithRoles: {
            params: {
                tenant_id: formats.VALIDATOR_ID,
                role: 'string|optional'
            },

            handler(ctx) {
                const conditions = {
                    'roles.tenant_id': ctx.params.tenant_id
                };

                if (ctx.params.role) {
                    conditions['roles.role'] = ctx.params.role;
                }

                return this.models.UserModel.query()
                    .withGraphJoined('roles')
                    .where(conditions)
                    .then(users => this.filterSecureFields(users, ['roles']));
            }
        },

        /**
         * Add a role for a tenent to a user
         *
         * @actions
         *
         * @param {String} user_id
         * @param {String} role_name
         * @param {String} tenant_id
         *
         * @returns {Object}
         */
        addRole: {
            params: {
                user_id: formats.VALIDATOR_ID,
                role_name: 'string',
                tenant_id: formats.VALIDATOR_NULLABLE_ID
            },

            handler(ctx) {
                return this._addRole(ctx.params.user_id, ctx.params.role_name, ctx.params.tenant_id);
            }
        },

        /**
         * Remove a role for a user
         *
         * @actions
         *
         * @param {String} user_id
         * @param {String} role_id
         * @param {String} tenant_id
         */
        removeRole: {
            params: {
                user_id: formats.VALIDATOR_ID,
                role_id: formats.VALIDATOR_ID,
                tenant_id: formats.VALIDATOR_ID
            },

            handler(ctx) {
                return this._removeRole(ctx.params.user_id, ctx.params.role_id, ctx.params.tenant_id);
            }
        },

        /**
         * Get user's roles
         *
         * @actions
         *
         * @param {String} user_id
         *
         * @returns {Array}
         */
        getRoles: {
            params: {
                user_id: formats.VALIDATOR_ID,
                role_name: 'string|optional',
                tenant_id: formats.VALIDATOR_OPTIONAL_ID
            },

            handler(ctx) {
                const query = this.models.UserModel.relatedQuery('roles')
                    .for(ctx.params.user_id);

                if (ctx.params.role_name !== undefined) {
                    query.where({ role: ctx.params.role_name });
                }

                if (ctx.params.tenant_id !== undefined) {
                    query.where({ tenant_id: ctx.params.tenant_id });
                }

                return query;
            }
        }
    },

    /**
     * Methods
     */
    methods: {
        /**
         * Ensure that the given password is meeting complexity requirements.
         *
         * @param {String} password
         */
        assertPasswordComplexity(password) {
            const entropy = stringEntropy(password);

            if (entropy < this.settings.minPasswordEntropy) {
                throw new ValidationError(
                    'Parameters validation error!',
                    null,
                    {
                        type: 'passwordEntropy',
                        message: `The 'password' field entropy must be greater than ${this.settings.minPasswordEntropy} bits.`,
                        field: 'password',
                        expected: this.settings.minPasswordEntropy,
                        actual: entropy
                    });
            }
        },

        /**
         * Get a user by email with its roles
         *
         * @param {String} email
         */
        _getOneWithRolesByEmail(email) {
            return this.models.UserModel.query()
                .findOne({email})
                .withGraphJoined('roles')
                .throwIfNotFound();
        },


        /**
         * Add a role for a tenent to a user
         *
         * @param {String} user_id
         * @param {String} role_name
         * @param {String} tenant_id
         */
        _addRole(user_id, role_name, tenant_id) {
            return this.models.UserModel.relatedQuery('roles')
                .for(user_id)
                .insert({
                    tenant_id: tenant_id,
                    role: role_name
                });
        },

        /**
         * Remove a role for a user
         *
         * @param {String} user_id
         * @param {String} role_id
         * @param {String} tenant_id
         */
        _removeRole(user_id, role_id, tenant_id) {
            return this.models.UserModel.relatedQuery('roles')
                .for(user_id)
                .deleteById(role_id)
                .where({ tenant_id })
                .throwIfNotFound();
        },

        /**
         * Generate the password hash for storage.
         *
         * @param {String} password
         */
        passwordKDF(password) {
            return Scrypt.kdf(password, { logN: this.settings.scryptN })
                .then((keyBuf) => {
                    return keyBuf.toString('base64');
                });
        },

        /**
         * Verify a password against the stored hash.
         *
         * @param {String} password
         */
        passwordVerify(scryptValue, password) {
            const keyBuf = Buffer.from(scryptValue, 'base64');
            return Scrypt.verify(keyBuf, password);
        },

        /**
         * Methods from vmol-objection@0.4
         */
        _update(params) {
            const id = params.id;
            delete params.id;

            return this.models.UserModel.query()
                .findById(id)
                .patch(params)
                .returning('*')
                .throwIfNotFound();
        },
    }
};
