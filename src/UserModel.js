'use strict';

const { BaseModel } = require('@voilab/vmol-objection');

const UsersRoleModel = require('./UsersRoleModel');
const SessionModel = require('./SessionModel');
const InvitationModel = require('./InvitationModel');

const formats = require('./lib/formats');

class UserModel extends BaseModel {
    static get tableName() {
        return 'users';
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['email'],
            properties: {
                id: formats.MODEL_ID,
                email: { type: 'string', format: 'email', maxLength: 254 },
                password: { type: 'string' }
            }
        };
    }

    static get relationMappings() {
        return {
            roles: {
                relation: BaseModel.HasManyRelation,
                modelClass: UsersRoleModel,
                join: {
                    from: 'users.id',
                    to: 'users_roles.user_id'
                }
            },

            sessions: {
                relation: BaseModel.HasManyRelation,
                modelClass: SessionModel,
                join: {
                    from: 'users.id',
                    to: 'users_sessions.user_id'
                }
            },

            invitations: {
                relation: BaseModel.HasManyRelation,
                modelClass: InvitationModel,
                join: {
                    from: 'users.id',
                    to: 'users_invitations.invited_by_user_id'
                }
            }
        };
    }

    $beforeUpdate() {
        this.updated_at = new Date().toISOString();
    }
}

module.exports = UserModel;
