const ID_TYPE = process.env['VMOL_USERS_ID_TYPE'] === 'uuid' ? 'uuid' : 'numeric';

const formats = {};

if (ID_TYPE === 'uuid') {
    formats.VALIDATOR_ID = 'uuid';
    formats.VALIDATOR_NULLABLE_ID = 'uuid|nullable';
    formats.VALIDATOR_OPTIONAL_ID = 'uuid|optional';

    formats.MODEL_ID = { type: 'string', format: 'uuid' };
    formats.MODEL_NULLABLE_ID = { type: ['string', 'null'], format: 'uuid' };
} else {
    formats.VALIDATOR_ID = 'string|numeric';
    formats.VALIDATOR_NULLABLE_ID = 'string|numeric|nullable';
    formats.VALIDATOR_OPTIONAL_ID = 'string|numeric|optional';

    formats.MODEL_ID = { type: 'string', pattern: '^[0-9]+$' };
    formats.MODEL_NULLABLE_ID = { type: ['string', 'null'], pattern: '^[0-9]+$' };
}

module.exports = formats;
