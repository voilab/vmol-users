'use strict';

const { MoleculerClientError } = require('moleculer').Errors;

const unflatten = require('flat').unflatten;
const { fn } = require('objection');
const { DBMiddleware } = require('@voilab/vmol-objection');

const jwtMixin = require('./mixins/jwt.mixin');

const formats = require('./lib/formats');
const { subject } = require('@casl/ability');

/**
 * Sessions Service definition
 */
module.exports = {
    /**
     * Mixins
     */
    mixins: [ DBMiddleware, jwtMixin ],

    /**
     * Settings
     */
    settings: {
        /**
         * Secret parameters will be ignored from the logs and denied to other services.
         */
        $secureSettings: [ 'jwt.privateKey', 'secureFields' ],

        /**
         * @type {Array} Hide fields from the responses.
         */
        secureFields: [ 'secret' ],

        jwt: {
            /**
             * @type {String} JWT Private key in PEM format
             */
            privateKey: null,

            /**
             * @type {String} JWT algorithm
             */
            algorithm: 'ES256',

            /**
             * @type {String} JWT Expiration time
             */
            expiresIn: '15m',

            /**
             * @type {String} JWT Issuer
             */
            issuer: 'SessionsService'
        },

        /**
         * @type {String} Users management service name
         */
        usersService: 'users',

        /**
         * @type {String} Session token subject name
         */
        delegationTokenSubject: 'SessionDelegation',

        sessionTokenExpiresIn: '15m',

        /**
         * @type {Object} Supported kind of sessions
         */
        sessionKinds: {
            userSession: 'userSession',
            delegationSession: 'delegationSession'
        }
    },

    /**
     * Actions
     */
    actions: {
        /**
         * Create a session.
         *
         * @actions
         *
         * @param {String} email User's email
         * @param {String} password User's password
         * @param {String} data Free form data. Can be used to identify the session for the user or to store non sensitive data.
         *
         * @returns {Object} Session.
         */
        create: {
            params: {
                email: 'email|normalize',
                password: 'string',
                data: 'object'
            },

            handler(ctx) {
                return ctx.call(`${this.settings.usersService}.getByCredentials`, { email: ctx.params.email, password: ctx.params.password })
                    .then(user => {
                        return this.models.SessionModel.query()
                            .insert({
                                user_id: user.id,
                                data: ctx.params.data,
                                kind: this.settings.sessionKinds.userSession
                            })
                            .returning('*'); // Fields are not filtered. We need the secret to set the session.
                    });
            }
        },

        /**
         * Create a new session from a delegation session token.
         *
         * @actions
         *
         * @param {String} token The delegation token issued by `createDelegationSession`
         * @param {String} data Free form data. Can be used to identify the session for the user or to store non sensitive data.
         *
         * @returns {Object} Session.
         */
        createFromDelegation: {
            params: {
                token: 'string',
                data: 'object'
            },

            handler(ctx) {
                const token = this.verifyToken(ctx.params.token, this.settings.delegationTokenSubject);

                return this.models.SessionModel.query()
                    .throwIfNotFound()
                    .findOne({
                        secret: token.secret,
                        kind: this.settings.sessionKinds.delegationSession, // Ensure the session can be used with a token
                        revoked_at: null // Ensure that the delegation session has not been revoked
                    }).then(delegation => {
                        this.touchSession(delegation.id);

                        return this.models.SessionModel.query()
                            .insert({
                                user_id: delegation.user_id,
                                delegation_id: delegation.id,
                                data: ctx.params.data,
                                kind: this.settings.sessionKinds.userSession
                            })
                            .returning('*'); // Fields are not filtered. We need the secret to set the session.
                    });
            }
        },

        /**
         * Create a delegation session.
         * Delegation sessions are a special kind of session that can be used to generate a new session without a user's credentials.
         * A delegation session can't be used directly to issue a token.
         *
         * @actions
         *
         * @param {String} user_id
         * @param {String} data Free form data. Can be used to identify the session for the user or to store non sensitive data.
         *
         * @returns {String} Encoded delegation JWT
         */
        createDelegation: {
            params: {
                user_id: formats.VALIDATOR_ID,
                data: 'object'
            },

            handler(ctx) {
                return this.models.SessionModel.query()
                    .insert({
                        user_id: ctx.params.user_id,
                        data: ctx.params.data,
                        kind: this.settings.sessionKinds.delegationSession
                    })
                    .returning('*')
                    .then(this.filterSecureFields);
            }
        },

        /**
         * Generate a new token for an existing delegation
         *
         * @actions
         *
         * @param {String} delegation_id
         * @param {String} user_id
         *
         * @returns {String} Encoded delegation JWT
         */
        getDelegationToken: {
            params: {
                delegation_id: formats.VALIDATOR_ID,
                user_id: formats.VALIDATOR_ID
            },

            handler(ctx) {
                return this.models.SessionModel.query()
                    .throwIfNotFound()
                    .findOne({
                        id: ctx.params.delegation_id,
                        user_id: ctx.params.user_id,
                        revoked_at: null,
                        kind: this.settings.sessionKinds.delegationSession
                    })
                    .then(this.signDelegationToken);
            }
        },

        /**
         * Issue a new short lived token for this session
         *
         * @actions
         *
         * @param {String} session_secret Session secret UUID
         *
         * @returns {String} JWT.
         */
        issueToken: {
            params: {
                session_secret: 'uuid'
            },

            handler(ctx) {
                return this.models.SessionModel.query()
                    .throwIfNotFound()
                    .findOne({
                        secret: ctx.params.session_secret,
                        revoked_at: null,
                        kind: this.settings.sessionKinds.userSession
                    })
                    .then(session => {
                        return ctx.call(`${this.settings.usersService}.getWithRoles`, { id: session.user_id })
                            .then(user => {
                                this.touchSession(session.id);
                                return this.signUserToken(user, session);
                            });
                    });
            }
        },

        /**
         * Issue a special kind of session token with a long expiration time
         *
         * Long session tokens are meant for internal use, when short lived tokens arn't practical or usable.
         * They can be used like short session tokens.
         * The first usage is to authenticate websocket connections.
         * During a WS handshake a normal token is received by the WS server.
         * The WS server will ask for a session token with a long expiration and store it on the socket metadata.
         * That session token will be used in lieu of the short token (likely expired by now) for every service calls.
         * Long session tokens SHOULD NOT leave the backend servers, unless you know exactly what you are doing.
         * In that case, Application tokens SHOULD be prefered.
         *
         * Long session tokens are issued based on the current short token and not the session secret.
         * Thus, this action SHOULD NOT be exposed through any external API and only be used internally.
         *
         * @actions
         *
         * @param {String}  expires_in  Token validity window
         *
         * @returns {String} JWT
         */
        issueLongSessionToken: {
            params: {
                $$strict: true,
                expires_in: 'string'
            },

            handler(ctx) {
                return this.models.SessionModel.query()
                    .throwIfNotFound()
                    .findOne({
                        id: ctx.locals.authn.sessid,
                        revoked_at: null,
                        kind: this.settings.sessionKinds.userSession
                    })
                    .then(session => {
                        return ctx.call(`${this.settings.usersService}.getWithRoles`, { id: session.user_id })
                            .then(user => this.signUserToken(user, session, ctx.params.expires_in));
                    });
            }
        },

        /**
         * Issue an application token
         *
         * Application tokens are special tokens which give access only to some select abilities.
         * They can be used to query an API, run scheduled tasks, etc.
         *
         * Requested abilities are checked against the current session.
         * Only abilities valid for this session will be accepted. Any and all privileges escalation attempts will result in an `ERR_FORBIDDEN` error.
         * Conditions are also checked and must either match the parent abilitie or be more restrictive.
         * Only fixed values for conditions are supported.
         *
         * @actions
         *
         * @param {String}  expires_in  Token validity window
         * @param {Array}   abilities   Collection of abilities to be included inside the token
         *
         * @returns {String} JWT
         */
        issueApplicationToken: {
            params: {
                $$strict: true,
                expires_in: 'string',
                abilities: {
                    type: 'array',
                    min: 1,
                    items: {
                        type: 'object',
                        props: {
                            subject: 'string',
                            action: 'string',
                            conditions: 'object|optional'
                        }
                    }
                }
            },

            handler(ctx) {
                for (const ability of ctx.params.abilities) {
                    // Check that all the conditions declared on the system abilities are listed in the requested token.
                    for (const rule of ctx.locals.authz.rulesFor(ability.action, ability.subject)) {
                        if (typeof rule.conditions === 'object') {
                            for (const field of Object.keys(rule.conditions)) {
                                if (typeof ability.conditions !== 'object' || !ability.conditions[field]) {
                                    throw new MoleculerClientError('Requested ability doesn\'t match any abilities and conditions on record');
                                }
                            }
                        }
                    }

                    if (!ability.conditions) {
                        // No conditions provided, check the action and subject alone.
                        // If the requested abality have conditions on our system, it would have failed before anyway.
                        if (!ctx.locals.authz.can(ability.action, ability.subject)) {
                            throw new MoleculerClientError('Forbidden', 403, 'ERR_FORBIDDEN', {
                                message: `Cannot execute "${ability.action}" on "${ability.subject}"`
                            });
                        }
                    } else {
                        // Enforce fixed conditions to avoid multiple tenants or other potential abuses.
                        for (const field in ability.conditions) {
                            if (typeof ability.conditions[field] !== 'string' && typeof ability.conditions[field] !== 'number') {
                                throw new MoleculerClientError('Conditions can only be fixed values', {
                                    field,
                                    condition: ability.conditions[field],
                                    type: typeof ability.conditions[field]
                                });
                            }
                        }

                        // Check the ability with all the conditions
                        const conditions = subject(ability.subject, unflatten(ability.conditions));
                        if (!ctx.locals.authz.can(ability.action, conditions)) {
                            throw new MoleculerClientError('Forbidden', 403, 'ERR_FORBIDDEN', {
                                message: `Cannot execute "${ability.action}" on "${ability.subject}" with conditions`,
                                conditions
                            });
                        }
                    }
                }

                return Promise.all([
                    ctx.call(`${this.settings.usersService}.getByToken`),
                    this.models.SessionModel.query()
                        .throwIfNotFound()
                        .findOne({
                            id: ctx.locals.authn.sessid,
                            revoked_at: null,
                            kind: this.settings.sessionKinds.userSession
                        })
                ])
                    .then(([user, session]) => {
                        return this.signApplicationToken(
                            user,
                            session,
                            ctx.params.abilities,
                            ctx.params.expires_in
                        );
                    });
            }
        },

        /**
         * List sessions for the current user
         *
         * @actions
         *
         * @param {Boolean} include_revoked Include revoked (non-valid) sesions
         *
         * @returns {Array} Sessions.
         */
        list: {
            params: {
                include_revoked: { type: 'boolean', default: false }
            },

            handler(ctx) {
                const conds = { // User can only list hist own sessions
                    user_id: ctx.locals.authn.id,
                    kind: this.settings.sessionKinds.userSession
                };

                if (!ctx.params.include_revoked) {
                    conds.revoked_at = null;
                }

                return this.models.SessionModel.query()
                    .where(conds)
                    .then(this.filterSecureFields);
            }
        },

        /**
         * List delegations for a user
         *
         * @actions
         *
         * @param {String} user_id
         *
         * @returns {Array} Delegations
         */
        listDelegations: {
            params: {
                user_id: formats.VALIDATOR_ID,
                include_revoked: { type: 'boolean', default: false }
            },

            handler(ctx) {
                const conds = {
                    user_id: ctx.params.user_id,
                    kind: this.settings.sessionKinds.delegationSession
                };

                if (!ctx.params.include_revoked) {
                    conds.revoked_at = null;
                }

                return this.models.SessionModel.query()
                    .where(conds)
                    .then(this.filterSecureFields);
            }
        },

        /**
         * Revoke a session for the current user
         *
         * @actions
         *
         * @param {String} session_id Session ID
         *
         * @throws {NotFoundError}
         */
        revoke: {
            params: {
                session_id: formats.VALIDATOR_OPTIONAL_ID
            },

            handler(ctx) {
                return this.models.SessionModel.query()
                    .where({ // User can only revoke his own sessions
                        id: ctx.params.session_id || ctx.locals.authn.sessid,
                        user_id: ctx.locals.authn.id,
                        kind: this.settings.sessionKinds.userSession    // Can only revoke user sessions
                    })
                    .patch({ revoked_at: fn.now() })
                    .throwIfNotFound()
                    .then(() => {
                        return { // Ask to purge the session if the current session is removed
                            purge: !ctx.params.session_id || ctx.params.session_id === ctx.locals.authn.sessid
                        };
                    });
            }
        },

        /**
         * Revoke a delegation and it's sessions
         *
         * @actions
         *
         * @param {String} delegation_id
         * @param {Boolean} revoke_user_sessions Revoke all user sessions linked to this delegation. default: false
         *
         * @throws {NotFoundError}
         */
        revokeDelegation: {
            params: {
                delegation_id: formats.VALIDATOR_ID,
                revoke_user_sessions: 'boolean|optional'
            },

            async handler(ctx) {
                if (ctx.params.revoke_user_sessions) {
                    await this.models.SessionModel.query()
                        .where({
                            delegation_id: ctx.params.delegation_id,
                            kind: this.settings.sessionKinds.userSession    // Can only revoke user sessions
                        })
                        .patch({ revoked_at: fn.now() });
                }

                return this.models.SessionModel.query()
                    .where({
                        id: ctx.params.delegation_id,
                        kind: this.settings.sessionKinds.delegationSession  // Can only revoke delegation sessions
                    })
                    .patch({ revoked_at: fn.now() })
                    .throwIfNotFound();
            }
        }
    },

    /**
     * Methods
     */
    methods: {
        /**
         * Sign a new short token.
         *
         * @param {Object} user The user object
         * @param {Object} session The session object
         * @param {String} expires_in Overides the token validity window
         */
        async signUserToken(user, session, expires_in = null) {
            const payload = {
                roles: user.roles.reduce(
                    (roles, role) => {
                        if (!roles[role.role]) {
                            roles[role.role] = [];
                        }

                        if (role.tenant_id) {
                            roles[role.role].push(role.tenant_id);
                        }

                        return roles;
                    },
                    {}
                ),
                email: user.email,
                ses: session.id
            };

            if (!expires_in) {
                expires_in = this.settings.sessionTokenExpiresIn;
            }

            if (typeof this.extendsTokenPayload === 'function') {
                return this.signToken(
                    user.id,
                    expires_in,
                    await this.extendsTokenPayload(user, payload)
                );
            } else {
                return this.signToken(user.id, this.settings.sessionTokenExpiresIn, payload);
            }
        },

        /**
         * Sign a new application token.
         *
         * @param {Object} user The user object
         * @param {Object} session The session object
         * @param {Array}  abilities
         * @param {}
         */
        signApplicationToken(user, session, abilities, expiresIn) {
            const payload = {
                email: user.email,
                ses: session.id,
                abilities
            };

            return this.signToken(
                user.id,
                expiresIn,
                payload
            );
        },

        /**
         * Sign a new delegation token.
         *
         * @param {Object} delegation
         */
        signDelegationToken(delegation) {
            return this.signToken(
                this.settings.delegationTokenSubject,
                null,
                { secret: delegation.secret }
            );
        },

        /**
         * Update the session's lastseen_at field.
         *
         * @param {String} sessid Session ID
         */
        touchSession(sessid) {
            return this.models.SessionModel.query()
                .throwIfNotFound()
                .findById(sessid)
                .patch({
                    lastseen_at: fn.now()
                })
                .returning('*');
        }
    }
};
