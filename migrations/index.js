'use strict';

module.exports = {
    'v0.1.0_DefaultSchema': require('./v0.1.0_DefaultSchema'),
    'v0.7.0_Delegation': require('./v0.7.0_Delegation'),
    'v0.9.0_InvitationRoles': require('./v0.9.0_InvitationRoles'),
    'v0.10.0_DefaultSchema_uuid': require('./v0.10.0_DefaultSchema_uuid')
};
