'use strict';

module.exports = {
    up(knex) {
        return knex.schema
            .createTable('users', table => {
                table.uuid('id').notNullable().defaultTo(knex.raw('gen_random_uuid()'));
                table.string('email', 254).notNullable();
                table.string('password', 128);

                table.primary('id');
                table.unique('email');
                table.timestamps(false, true);
            })
            .createTable('users_roles', table => {
                table.uuid('id').notNullable().defaultTo(knex.raw('gen_random_uuid()'));
                table.uuid('tenant_id').nullable();
                table.uuid('user_id');
                table.string('role', 128).notNullable();

                table.primary('id');
                table.index(['tenant_id', 'user_id']);
                table.timestamps(false, true);

                table.foreign('user_id').references('users.id');
            })
            .createTable('users_sessions', table => {
                table.uuid('id').notNullable().defaultTo(knex.raw('gen_random_uuid()'));
                table.uuid('secret').notNullable().defaultTo(knex.raw('gen_random_uuid()'));
                table.uuid('user_id');
                table.json('data');
                table.string('kind', 128).notNullable().defaultTo('userSession');
                table.uuid('delegation_id').nullable();
                table.timestamp('revoked_at');
                table.timestamp('lastseen_at').defaultTo(knex.fn.now());

                table.primary('id');
                table.unique('secret');
                table.index('user_id');
                table.index('delegation_id');
                table.timestamps(false, true);

                table.foreign('user_id').references('users.id');
                table.foreign('delegation_id').references('users_sessions.id');
            })
            .createTable('users_invitations', table => {
                table.uuid('id').notNullable().defaultTo(knex.raw('gen_random_uuid()'));
                table.uuid('tenant_id');
                table.uuid('invited_by_user_id');
                table.string('email', 254).notNullable();
                table.specificType('roles', 'string[]').notNullable();
                table.timestamp('revoked_at');
                table.timestamp('accepted_at');

                table.primary('id');
                table.index(['tenant_id', 'invited_by_user_id']);
                table.timestamps(false, true);

                table.foreign('invited_by_user_id').references('users.id');
            });
    },

    down(knex) {
        return knex.schema
            .dropTable('users_invitations')
            .dropTable('users_sessions')
            .dropTable('users_roles')
            .dropTable('users');
    }
};
