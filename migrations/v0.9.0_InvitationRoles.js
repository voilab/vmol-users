'use strict';

module.exports = {
    async up(knex) {
        // create new column for multiple roles. Keep nullable since
        // we will add value in step 2
        await knex.schema.alterTable('users_invitations', table => {
            table.specificType('roles', 'string[]');
        });
        // step 2 : set unique role in each invitation roles array
        await knex('users_invitations').update({
            roles: knex.raw('ARRAY[role]')
        });
        // step 3 : set new column as notnull and remove old one
        return knex.schema.alterTable('users_invitations', table => {
            table.dropNullable('roles');
            table.dropColumn('role');
        });
    },

    async down(knex) {
        // create new column for unique role. Keep nullable since
        // we will add value in step 2
        await knex.schema.alterTable('users_invitations', table => {
            table.string('role', 128);
        });
        // step 2 : set unique role with first role in array
        await knex('users_invitations').update({
            role: knex.raw('roles[1]')
        });
        // step 3 : set new column as notnull and remove old one
        return knex.schema.alterTable('users_invitations', table => {
            table.dropNullable('role');
            table.dropColumn('roles');
        });
    }
};
