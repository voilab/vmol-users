'use strict';

module.exports = {
    up(knex) {
        return knex.schema
            .createTable('users', table => {
                table.increments('id');
                table.string('email', 254).notNullable();
                table.unique('email');
                table.string('password', 128);
                table.timestamps(false, true);
            })
            .createTable('users_roles', table => {
                table.increments('id');
                table.integer('user_id').unsigned();
                table.integer('tenant_id').unsigned().nullable();
                table.index('tenant_id');
                table.string('role', 128).notNullable();
                table.timestamps(false, true);
                table.foreign('user_id').references('users.id');
            })
            .createTable('users_sessions', table => {
                table.increments('id');
                table.uuid('secret').notNullable().defaultTo(knex.raw('gen_random_uuid()'));
                table.unique('secret');
                table.integer('user_id').unsigned();
                table.json('data');
                table.timestamp('revoked_at');
                table.timestamp('lastseen_at').defaultTo(knex.fn.now());
                table.timestamps(false, true);
                table.foreign('user_id').references('users.id');
            })
            .createTable('users_invitations', table => {
                table.increments('id');
                table.integer('invited_by_user_id').unsigned();
                table.integer('tenant_id').unsigned();
                table.string('email', 254).notNullable();
                table.specificType('roles', 'string[]').notNullable();
                table.timestamp('revoked_at');
                table.timestamp('accepted_at');
                table.timestamps(false, true);
                table.foreign('invited_by_user_id').references('users.id');
            });
    },

    down(knex) {
        return knex.schema
            .dropTable('users_invitations')
            .dropTable('users_sessions')
            .dropTable('users_roles')
            .dropTable('users');
    }
};
