'use strict';

module.exports = {
    up(knex) {
        return knex.schema
            .table('users_sessions', table => {
                table.string('kind', 128).notNullable().defaultTo('userSession');
                table.integer('delegation_id').unsigned().nullable();
                table.index('delegation_id');
                table.foreign('delegation_id').references('users_sessions.id');
            });
    },

    down(knex) {
        return knex.schema
            .table('users_sessions', table => {
                table.dropForeign('delegation_id');
                table.dropColumn('delegation_id');
                table.dropColumn('kind');
            });
    }
};
